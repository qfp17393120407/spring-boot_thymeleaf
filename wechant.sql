-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: wechat
-- ------------------------------------------------------
-- Server version	5.7.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(20) NOT NULL COMMENT '菜单名称',
  `menu_desc` varchar(50) DEFAULT NULL COMMENT '菜单描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  `parent_id` varchar(20) DEFAULT NULL COMMENT '父级菜单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'系统管理','系统管理','2023-05-13 00:23:32','2023-05-13 01:08:16','查询角色','admin',NULL),(2,'菜单管理','菜单管理','2023-05-12 09:49:52','2023-05-12 14:56:26','admin','admin','1'),(3,'用户管理','用户管理','2023-05-12 09:50:18','2023-05-12 14:56:58','admin','admin','1'),(4,'权限管理','权限管理','2023-05-12 10:02:49','2023-05-12 14:57:22','admin','admin','1'),(5,'角色管理','角色管理','2023-05-12 14:59:12','2023-05-13 01:08:53','admin','admin','1');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_permission`
--

DROP TABLE IF EXISTS `menu_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) NOT NULL COMMENT '菜单id',
  `permission_code_list` varchar(200) NOT NULL COMMENT '权限编码集合,逗号分割',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_permission`
--

LOCK TABLES `menu_permission` WRITE;
/*!40000 ALTER TABLE `menu_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(20) NOT NULL COMMENT '权限名称',
  `permission_code` varchar(200) NOT NULL COMMENT '权限编码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  `menu_id` int(10) DEFAULT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (1,'用户查询','user:select','2023-05-08 15:38:08','2023-05-08 15:38:08','admin','admin',NULL),(2,'用户添加或修改','user:save','2023-05-08 15:38:08','2023-05-08 15:38:08','admin','admin',NULL),(3,'用户删除','user:delete','2023-05-08 15:38:08','2023-05-08 15:38:08','admin','admin',NULL),(4,'权限保存','permission:save','2023-05-08 15:38:08','2023-05-08 15:38:08','admin','admin',NULL),(5,'权限删除','permission:delete','2023-05-08 15:38:08','2023-05-08 15:38:08','admin','admin',NULL),(6,'权限查询','permission:select','2023-05-08 15:38:08','2023-05-08 15:38:08','admin','admin',NULL),(7,'角色查询','role:select','2023-05-10 16:32:00','2023-05-10 16:32:00','admin','admin',NULL),(8,'角色保存','role:save','2023-05-10 16:36:25','2023-05-12 15:09:51','admin','admin',NULL),(9,'角色删除','role:delete','2023-05-10 16:45:12','2023-05-12 15:09:36','admin','admin',NULL),(10,'菜单查询','menu:select','2023-05-11 09:18:54','2023-05-12 15:10:07','admin','admin',NULL),(12,'菜单添加','menu:save','2023-05-11 19:03:48','2023-05-12 15:10:35','admin','admin',NULL),(13,'菜单删除','menu:delete','2023-05-11 19:04:20','2023-05-12 15:10:47','admin','admin',NULL),(15,'用户角色查询','userrole:select','2023-05-12 17:23:48','2023-05-12 17:23:48','admin','admin',NULL),(16,'用户角色保存','userrole:save','2023-05-12 17:24:12','2023-05-12 17:24:12','admin','admin',NULL),(17,'用户角色删除','userrole:delete','2023-05-12 17:24:29','2023-05-12 17:24:29','admin','admin',NULL),(18,'角色菜单查询','rolemenu:select','2023-05-12 17:56:49','2023-05-12 17:56:49','admin','admin',NULL),(19,'角色菜单保存','rolemenu:save','2023-05-12 17:57:11','2023-05-12 17:57:11','admin','admin',NULL),(20,'角色菜单删除','rolemenu:delete','2023-05-12 17:57:29','2023-05-12 17:57:29','admin','admin',NULL);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) NOT NULL COMMENT '角色名',
  `role_code` varchar(200) NOT NULL COMMENT '角色编码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'查询','ROLE_QUERY','2023-05-08 15:29:09','2023-05-08 15:29:09','admin','admin'),(2,'操作员','ROLE_OPT','2023-05-08 15:29:09','2023-05-08 15:29:09','admin','admin'),(3,'审核员','ROLE_AUTH','2023-05-08 15:29:09','2023-05-08 15:29:09','admin','admin'),(5,'分配用户','ROLE_USER_QUERY','2023-05-12 15:07:47','2023-05-12 15:07:47','admin','admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_menu`
--

DROP TABLE IF EXISTS `role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL COMMENT '角色id',
  `menu_id` int(10) NOT NULL COMMENT '菜单id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  `permission_code_list` varchar(200) NOT NULL COMMENT '权限编码集合,逗号分割',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_menu`
--

LOCK TABLES `role_menu` WRITE;
/*!40000 ALTER TABLE `role_menu` DISABLE KEYS */;
INSERT INTO `role_menu` VALUES (1,1,2,'2023-05-12 18:13:10','2023-05-13 00:18:43','admin','admin','menu:select'),(2,1,3,'2023-05-12 18:13:28','2023-05-13 00:18:49','admin','admin','user:select'),(3,1,5,'2023-05-12 18:13:51','2023-05-13 01:09:33','admin','admin','role:select'),(4,1,4,'2023-05-12 18:15:47','2023-05-13 00:19:05','admin','admin','permission:select');
/*!40000 ALTER TABLE `role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  `user_type` char(2) DEFAULT '0' COMMENT '用户类型，0-普通用户，1-超级管理员',
  `group_id` int(10) DEFAULT NULL COMMENT '分组id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2031935499 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','$2a$10$3DedH3KXmIwkcx9/hM7VJuA.jDu3q0KbBXv442JTGRhoDEzWSXBY.','13099809987','2023-03-09 09:09:09','2023-05-09 16:17:13','admin','admin','1',0),(2,'qiao','$2a$10$IgadnUdI.dvnA7URaeqGH.nUOYhySyq3FkgFicKTZfN2NA91J97gS','13088765436','2023-03-09 09:09:09','2023-03-09 09:09:09','admin','admin','0',NULL),(3,'wang','$2a$10$v/34enCB3faJrMpMA2.Us.YqGH7VugOECHJZGNrp8xK3iSkZScHPq','13894998738','2023-05-09 15:33:50','2023-05-09 16:06:09','admin','admin','0',0),(4,'liuyue','$2a$10$1e96XDR9HhSSSyBu3nM2kuumtUn72M/2wb1GOb8mmrjxAOiqWL186','17393120422','2023-05-10 15:01:24','2023-05-10 15:01:24','admin','admin','0',NULL),(5,'liming','$2a$10$Zog4F1eNergYcg/4WFpOBei5h.d7c1ZuGBA2vPSI4pgyDswWRUlmK','13897657635','2023-05-10 14:45:05','2023-05-10 14:45:05','admin','admin','0',NULL),(6,'zhang','$2a$10$hs9WvAd4Us4F1f6usMcCCuLWR6Wq8KH1iOI47Q3ee8Z8AhCtAb7DO','17393120423','2023-05-10 14:44:20','2023-05-10 14:44:20','admin','admin','0',NULL),(7,'sirui','$2a$10$rWTo5rlM9B9rcrdZyDfN.OoesrpZu9foUIL/2k5L7sviRYXmO0qoC','13897657633','2023-05-10 15:06:12','2023-05-10 15:06:12','admin','admin','0',NULL),(2031935491,'查询角色','$2a$10$VHDo3qASE28DxpwhDie9GunVHWMHiH5FdsXhndemUltSBSlftoWc.','17393120423','2023-05-10 15:07:22','2023-05-12 18:16:25','admin','admin','0',NULL),(2031935492,'黎明','$2a$10$XagYiMeHFBnATHSvxtjpHex2jOaR274MG9DVqDCk/s9vjLrOdRCfy','17393120423','2023-05-11 10:24:38','2023-05-12 14:57:42','admin','admin','0',NULL),(2031935498,'测试','$2a$10$LAlin.O6zTpro69ynMNblOzdjpB/K8jifYgEo7LiEee5IAoJ06phG','17393120423','2023-05-12 10:51:45','2023-05-12 10:51:45','admin','admin','0',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_group` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` int(10) NOT NULL COMMENT '分组名称',
  `group_desc` int(10) NOT NULL COMMENT '分组描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `role_id` int(10) NOT NULL COMMENT '角色id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` varchar(20) NOT NULL COMMENT '创建用户',
  `update_user` varchar(20) NOT NULL COMMENT '更新用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,2031935491,1,'2023-05-12 18:21:23','2023-05-13 00:32:56','admin','admin');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-13 10:47:40
