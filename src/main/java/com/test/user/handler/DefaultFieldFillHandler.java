package com.test.user.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.test.user.entity.AbstractEntity;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.Objects;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-09 15:16
 */
@Component
public class DefaultFieldFillHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        if (Objects.nonNull(metaObject) && metaObject.getOriginalObject() instanceof AbstractEntity){
            AbstractEntity abstractEntity = (AbstractEntity)metaObject.getOriginalObject();
            Date now = new Date();
            abstractEntity.setCreateTime(now);
            abstractEntity.setUpdateTime(now);

            String username = getLoginUserName();
            abstractEntity.setCreateUser(username);
            abstractEntity.setUpdateUser(username);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Object updateTime = getFieldValByName("updateTime", metaObject);
        if (Objects.isNull(updateTime)){
            setFieldValByName("updateTime",new Date(),metaObject);
        }

        Object updateUser = getFieldValByName("updateUser", metaObject);
        if (Objects.isNull(updateUser)){
            setFieldValByName("updateUser",getLoginUserName(),metaObject);
        }
    }

    public String getLoginUserName(){
        String username = "anonymous";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            username = authentication.getName();
        }
        return username;
    }
}
