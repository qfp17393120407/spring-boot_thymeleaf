package com.test.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.user.entity.RoleMenu;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-11 17:04
 */
public interface RoleMenuService extends IService<RoleMenu> {
    List<RoleMenu> getList();
}
