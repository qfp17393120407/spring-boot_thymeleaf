package com.test.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.user.entity.Role;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:28
 */
public interface RoleService extends IService<Role> {
    List<Role> getList();
}
