package com.test.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.test.user.entity.User;

import java.util.List;
import java.util.Map;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 14:44
 */
public interface UserService extends IService<User> {

    List<User> getUserList();

    void delete(Integer userId);
}
