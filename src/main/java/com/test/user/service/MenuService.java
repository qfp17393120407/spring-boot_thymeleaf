package com.test.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.user.entity.Menu;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-11 16:44
 */
public interface MenuService extends IService<Menu> {
    List<Menu> getList();
}
