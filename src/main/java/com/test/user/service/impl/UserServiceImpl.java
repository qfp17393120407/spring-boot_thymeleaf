package com.test.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.user.entity.CustomerUserDetails;
import com.test.user.entity.RoleMenu;
import com.test.user.entity.User;
import com.test.user.entity.UserRole;
import com.test.user.enums.UserTypeEnum;
import com.test.user.mapper.RoleMenuMapper;
import com.test.user.mapper.UserMapper;
import com.test.user.mapper.UserRoleMapper;
import com.test.user.service.RoleMenuService;
import com.test.user.service.UserService;
import com.test.user.vo.RoleVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 14:45
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserDetailsService, UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;
    
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUsername,username);
        User user = userMapper.selectOne(queryWrapper);
        if (null == user){
            log.error("用户名或密码错误");
            throw  new UsernameNotFoundException("用户名或密码错误");
        }

        //查询角色及权限
        List<String> authoritiesList =  new ArrayList<>();
        Integer userId = user.getId();
        String userType = user.getUserType();
        log.info("userType:{}",userType.toString());

        List<RoleVo> roleVos = userMapper.selectRole(userId);

        List<String> authPermissions = new ArrayList<>();

        List<String> roleList = new ArrayList<>();
        List<String> finalRoleList = roleList;
        List<String> finalAuthPermissions = authPermissions;
        roleVos.stream().forEach(vo->{
            finalRoleList.add(vo.getRoleCode());
            Integer roleId = vo.getRoleId();
            List<String> stringList = roleMenuMapper.selectRoleCodesByRoleID(roleId);
            List<String> permissions = new ArrayList<>();
            stringList.stream().forEach(list->{
                if (StringUtils.isNotBlank(list)){
                    permissions.addAll(stringToList(list));
                }
            });
            finalAuthPermissions.addAll(permissions);
        });

        if (UserTypeEnum.ROOT.getCode().equals(userType)){
            authPermissions = userMapper.selectAllPermission();
            roleList = userMapper.selectAllRole();
            authoritiesList.addAll(authPermissions);
            authoritiesList.addAll(roleList);
        }else {
            authoritiesList.addAll(finalAuthPermissions);
            authoritiesList.addAll(finalRoleList);
        }
        log.info("{}的权限:{}",user.getUsername(),authPermissions.toString());

        log.info("{}的角色:{}",user.getUsername(),roleList.toString());

        authoritiesList = authoritiesList.stream().distinct().collect(Collectors.toList());
        CustomerUserDetails customerUserDetails = new CustomerUserDetails(user, authoritiesList);
        return customerUserDetails;
    }

    public List<String> stringToList(String list){
        return Arrays.asList(list.split(","));
    }

    @Override
    public List<User> getUserList() {
        return userMapper.selectList(null);
    }

    @Override
    @Transactional
    public void delete(Integer userId) {
        //1.删除该用户关联的角色菜单记录
        LambdaQueryWrapper<UserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserRole::getUserId,userId);
        int delete = userRoleMapper.delete(queryWrapper);

        log.info("删除了{}条记录",delete);
        //2.删除用户
        userMapper.deleteById(userId);
    }
}
