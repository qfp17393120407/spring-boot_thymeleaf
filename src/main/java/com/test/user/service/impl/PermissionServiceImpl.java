package com.test.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.user.entity.Permission;
import com.test.user.mapper.PermissionMapper;
import com.test.user.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:33
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> getList() {
        return permissionMapper.selectList(null);
    }

    @Override
    public void deleteById(Integer id) {
        permissionMapper.deleteById(id);
    }
}
