package com.test.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.user.entity.Role;
import com.test.user.entity.RoleMenu;
import com.test.user.mapper.RoleMapper;
import com.test.user.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:29
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> getList() {
        return roleMapper.selectList(null);
    }
}
