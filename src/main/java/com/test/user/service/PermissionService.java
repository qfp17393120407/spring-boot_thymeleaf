package com.test.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.user.entity.Permission;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:33
 */
public interface PermissionService extends IService<Permission> {
    List<Permission> getList();

    void deleteById(Integer id);
}
