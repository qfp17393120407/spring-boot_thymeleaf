package com.test.user.enums;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-08 16:51
 */
public enum UserTypeEnum {
    ROOT("1","超级管理员"),

    NORMAL("0","普通用户")
    ;

    private String code;

    private String msg;

    UserTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
