package com.test.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.user.entity.RoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-11 17:03
 */
@Repository
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    @Select("select permission_code_list from role_menu where role_id =#{roleId}")
    List<String> selectRoleCodesByRoleID(Integer roleId);
}
