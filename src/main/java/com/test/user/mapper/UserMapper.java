package com.test.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.user.entity.User;
import com.test.user.vo.RoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 14:42
 */
@Repository
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select("select ur.role_id,r.role_code from user_role ur join role r on ur.role_id = r.id where user_id = #{userId} ")
    List<RoleVo> selectRole(Integer userId);

    @Select("select distinct(permission_code) from permission")
    List<String> selectAllPermission();

    @Select("select distinct(role_code) from role")
    List<String> selectAllRole();
}
