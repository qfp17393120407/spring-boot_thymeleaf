package com.test.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.user.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:30
 */
@Repository
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
}
