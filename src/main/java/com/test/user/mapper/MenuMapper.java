package com.test.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.user.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-11 16:44
 */
@Repository
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
}
