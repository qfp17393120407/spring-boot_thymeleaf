package com.test.user.vo;

import lombok.Data;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-12 20:03
 */
@Data
public class RoleVo {
    private Integer roleId;

    private String roleCode;
}
