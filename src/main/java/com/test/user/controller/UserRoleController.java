package com.test.user.controller;

import com.test.user.entity.UserRole;
import com.test.user.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:23
 */
@Controller
@RequestMapping("userRole")
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @PreAuthorize("hasAnyAuthority('userrole:select')")
    @RequestMapping("toList")
    public String toList(Model model){
        List<UserRole> list = userRoleService.getList();
        model.addAttribute("list",list);
        return "userRoleList";
    }

    @PreAuthorize("hasAnyAuthority('userrole:save')")
    @RequestMapping("toAddUserRole")
    public String toAddUserRole(){
        return "addUserRole";
    }

    @PreAuthorize("hasAnyAuthority('userrole:save')")
    @RequestMapping("addUserRole")
    public String addUserRole(UserRole userRole){
        userRoleService.save(userRole);
        return "redirect:/userRole/toList";
    }

    @PreAuthorize("hasAnyAuthority('userrole:save')")
    @RequestMapping("toUpdateUserRole")
    public String toUpdateUserRole(Integer id,Model model){
        UserRole userRole = userRoleService.getById(id);
        model.addAttribute("userRole",userRole);
        return "updateUserRole";
    }

    @PreAuthorize("hasAnyAuthority('userrole:save')")
    @RequestMapping("updateUserRole")
    public String updateUserRole(UserRole userRole){
        userRoleService.saveOrUpdate(userRole);
        return "redirect:/userRole/toList";
    }

    @PreAuthorize("hasAnyAuthority('userrole:delete')")
    @RequestMapping("delete")
    public String delete(Integer id){
        userRoleService.removeById(id);
        return "redirect:/userRole/toList";
    }
}
