package com.test.user.controller;

import com.test.user.entity.Menu;
import com.test.user.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-11 16:12
 */
@Controller
@RequestMapping("menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @PreAuthorize("hasAnyAuthority('menu:select')")
    @RequestMapping("toList")
    public String toList(Model model){
        List<Menu> menuList = menuService.getList();
        model.addAttribute("menuList",menuList);
        return "menuList";
    }

    @PreAuthorize("hasAnyAuthority('menu:save')")
    @RequestMapping("toAddMenu")
    public String toAddMenu(){
        return "addMenu";
    }

    @PreAuthorize("hasAnyAuthority('menu:save')")
    @RequestMapping("addMenu")
    public String addMenu(Menu menu){
        menuService.save(menu);
        return "redirect:/menu/toList";
    }

    @PreAuthorize("hasAnyAuthority('menu:save')")
    @RequestMapping("toUpdateMenu")
    public String toUpdateMenu(Integer id,Model model){
        Menu menu = menuService.getById(id);
        model.addAttribute("menu",menu);
        return "updateMenu";
    }

    @PreAuthorize("hasAnyAuthority('menu:save')")
    @RequestMapping("updateMenu")
    public String updateMenu(Menu menu){
        menuService.saveOrUpdate(menu);
        return "redirect:/menu/toList";
    }

    @PreAuthorize("hasAnyAuthority('menu:delete')")
    @RequestMapping("delete")
    public String delete(Integer id){
        menuService.removeById(id);
        return "redirect:/menu/toList";
    }

}
