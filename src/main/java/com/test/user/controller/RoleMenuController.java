package com.test.user.controller;

import com.test.user.entity.RoleMenu;
import com.test.user.service.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:24
 */
@Controller
@RequestMapping("roleMenu")
public class RoleMenuController {

    @Autowired
    private RoleMenuService roleMenuService;

    @PreAuthorize("hasAnyAuthority('rolemenu:select')")
    @RequestMapping("toList")
    public String toList(Model model){
        List<RoleMenu> list = roleMenuService.getList();
        model.addAttribute("list",list);
        return "roleMenuList";
    }

    @PreAuthorize("hasAnyAuthority('rolemenu:save')")
    @RequestMapping("toAddRoleMenu")
    public String toAddRoleMenu(){
        return "addRoleMenu";
    }

    @PreAuthorize("hasAnyAuthority('rolemenu:save')")
    @RequestMapping("addRoleMenu")
    public String addRoleMenu(RoleMenu roleMenu){
        roleMenuService.save(roleMenu);
        return "redirect:/roleMenu/toList";
    }

    @PreAuthorize("hasAnyAuthority('rolemenu:save')")
    @RequestMapping("toUpdateRoleMenu")
    public String toUpdateRoleMenu(Integer id,Model model){
        RoleMenu roleMenu = roleMenuService.getById(id);
        model.addAttribute("roleMenu",roleMenu);
        return "updateRoleMenu";
    }

    @PreAuthorize("hasAnyAuthority('rolemenu:save')")
    @RequestMapping("updateRoleMenu")
    public String updateRoleMenu(RoleMenu roleMenu){
        roleMenuService.saveOrUpdate(roleMenu);
        return "redirect:/roleMenu/toList";
    }

    @PreAuthorize("hasAnyAuthority('rolemenu:delete')")
    @RequestMapping("delete")
    public String delete(Integer id){
        roleMenuService.removeById(id);
        return "redirect:/roleMenu/toList";
    }
}
