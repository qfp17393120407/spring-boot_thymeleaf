package com.test.user.controller;

import com.test.user.entity.User;
import com.test.user.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 14:35
 */

@Api(tags = "用户管理")
@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PreAuthorize("hasAnyAuthority('user:select')")
    @RequestMapping("/toUserList")
    public String toUserList(Model model){
        List<User> userList = userService.getUserList();
        model.addAttribute("userList",userList);
        return "userList";
    }

    @PreAuthorize("hasAnyAuthority('user:save')")
    @RequestMapping("/toAddUser")
    public String toSave(){
        return "addUser";
    }

    @PreAuthorize("hasAnyAuthority('user:save')")
    @RequestMapping("/addUser")
    public String save(User user){
        String password = user.getPassword();
        user.setPassword(passwordEncoder.encode(password));
        userService.saveOrUpdate(user);
        return "redirect:/toUserList";
    }

    @PreAuthorize("hasAnyAuthority('user:save')")
    @RequestMapping("/toEditUser")
    public String toUpdateUser(Integer id,Model model) {
        User user=userService.getById(id);
        System.out.println("id="+user.getId());
        model.addAttribute("user",user);
        return "updateUser";
    }

    @PreAuthorize("hasAnyAuthority('user:save')")
    @RequestMapping("/updateUser")
    public String updateUser(User user) {
        userService.saveOrUpdate(user);
        return "redirect:/toUserList";
    }


    @PreAuthorize("hasAnyAuthority('user:delete')")
    @RequestMapping("/delete")
    public String delete(Integer id){
        userService.delete(id);
        return "redirect:/toUserList";
    }
}
