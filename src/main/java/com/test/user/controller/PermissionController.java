package com.test.user.controller;

import com.test.user.entity.Permission;
import com.test.user.service.PermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:18
 */
@Controller
@RequestMapping("permission")
@Slf4j
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @PreAuthorize("hasAnyAuthority('permission:select')")
    @RequestMapping("toList")
    public String toList(Model model){
        List<Permission> permissionList = permissionService.getList();
        model.addAttribute("permissionList",permissionList);
        log.info("permissionList{}",permissionList.toString());
        return "permissionList";
    }

    @PreAuthorize("hasAnyAuthority('permission:save')")
    @RequestMapping("toAdd")
    public String toAdd(){
        return "addPermission";
    }

    @PreAuthorize("hasAnyAuthority('permission:save')")
    @RequestMapping("addPermission")
    public String add(Permission permission){
        permissionService.save(permission);
        return "redirect:/permission/toList";
    }

    @PreAuthorize("hasAnyAuthority('permission:save')")
    @RequestMapping("toUpdate")
    public String toUpdate(Integer id,Model model){
        Permission permission = permissionService.getById(id);
        model.addAttribute("permission",permission);
        return "updatePermission";
    }

    @PreAuthorize("hasAnyAuthority('permission:save')")
    @RequestMapping("updatePermission")
    public String update(Permission permission){
        permissionService.saveOrUpdate(permission);
        return "redirect:/permission/toList";
    }

    @PreAuthorize("hasAnyAuthority('permission:delete')")
    @RequestMapping("delete")
    public String delete(Integer id){
        permissionService.deleteById(id);
        return "redirect:/permission/toList";
    }
}
