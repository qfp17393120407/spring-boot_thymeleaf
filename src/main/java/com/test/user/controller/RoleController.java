package com.test.user.controller;

import com.test.user.entity.Role;
import com.test.user.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-10 15:18
 */
@Controller
@RequestMapping("role")
public class RoleController {

    @Autowired
    private RoleService roleService;


    @PreAuthorize("hasAnyAuthority('role:select')")
    @RequestMapping("toList")
    public String toList(Model model){
        List<Role> roleList = roleService.getList();
        model.addAttribute("roleList",roleList);
        return "roleList";
    }

    @PreAuthorize("hasAnyAuthority('role:save')")
    @RequestMapping("toAddRole")
    public String toAddRole(){
        return "addRole";
    }

    @PreAuthorize("hasAnyAuthority('role:save')")
    @RequestMapping("addRole")
    public String addRole(Role role){
        roleService.save(role);
        return "redirect:/role/toList";
    }

    @PreAuthorize("hasAnyAuthority('role:save')")
    @RequestMapping("toUpdateRole")
    public String toUpdateRole(Integer id,Model model){
        Role role = roleService.getById(id);
        model.addAttribute("role",role);
        return "updateRole";
    }

    @PreAuthorize("hasAnyAuthority('role:save')")
    @RequestMapping("updateRole")
    public String updateRole(Role role){
        roleService.saveOrUpdate(role);
        return "redirect:/role/toList";
    }

    @PreAuthorize("hasAnyAuthority('role:delete')")
    @RequestMapping("delete")
    public String delete(Integer id){
        roleService.removeById(id);
        return "redirect:/role/toList";
    }

}
