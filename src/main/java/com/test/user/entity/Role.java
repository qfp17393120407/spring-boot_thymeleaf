package com.test.user.entity;

import lombok.Data;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 14:05
 */
@Data
public class Role extends AbstractEntity{

    private String roleName;

    private String roleCode;
}
