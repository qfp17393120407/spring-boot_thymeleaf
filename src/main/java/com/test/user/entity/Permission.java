package com.test.user.entity;

import lombok.Data;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 14:06
 */
@Data
public class Permission extends AbstractEntity{

    private String permissionName;

    private String permissionCode;
}
