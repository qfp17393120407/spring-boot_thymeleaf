package com.test.user.entity;

import lombok.Data;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-11 16:22
 */
@Data
public class RoleMenu extends AbstractEntity {

    private Integer roleId;

    private Integer menuId;

    private String permissionCodeList;
}
