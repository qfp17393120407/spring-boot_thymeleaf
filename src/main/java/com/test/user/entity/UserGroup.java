package com.test.user.entity;

import lombok.Data;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-08 16:49
 */
@Data
public class UserGroup extends AbstractEntity {

    private String groupName;

    private String groupDesc;
}
