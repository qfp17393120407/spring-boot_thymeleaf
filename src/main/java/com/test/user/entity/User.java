package com.test.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 10:06
 */
@Data
@TableName("user")
public class User extends AbstractEntity{

    @NotBlank(message = "请输入用户名")
    @Length(message = "不能超过 {max} 个字符",max = 20)
    private String username;

    @NotBlank(message = "请输入密码")
    @Length(message = "最少为{min}个字符",min = 6)
    private String password;

    @NotBlank(message = "请输入手机号")
    @Pattern(regexp = "^1[34578][0-9]{9}$",message = "请输入正确的手机号")
    private String phone;

    private Integer groupId;

    private String userType;

}
