package com.test.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import java.util.List;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-11 16:12
 */
@Data
public class Menu extends AbstractEntity {

    private Integer parentId;

    private String menuName;

    private String menuDesc;

    @TableField(exist = false)
    private List<Permission> permissionList;
}
