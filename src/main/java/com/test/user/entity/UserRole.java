package com.test.user.entity;

import lombok.Data;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 15:24
 */
@Data
public class UserRole extends AbstractEntity {
    private Integer userId;

    private Integer roleId;
}
