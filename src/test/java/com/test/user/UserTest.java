package com.test.user;

import com.test.user.config.SecurityConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author清梦
 * @site www.xiaomage.com
 * @company xxx公司
 * @create 2023-05-06 18:15
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserTest {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void test1(){
        /*BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode("123456");
        System.out.println("password:123456="+encode);
        String encode1 = encoder.encode("123456");
        System.out.println("password1:123456="+encode1);

        boolean b = passwordEncoder.matches(encode, encode1);
        System.out.println("b:"+b);
*/

        String phone = "13099309377";
        ///^1[34578]\d{9}$/
        boolean b1= phone.matches("^1[34578][0-9]{9}$");
        if (b1){
            System.out.println(" this is phone");
        }else {
            System.out.println("phone error");
        }

    }
}
