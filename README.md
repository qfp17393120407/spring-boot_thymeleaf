# SpringBoot_thymeleaf

#### 介绍
以springboot，security，mybatisPlus及thymeleaf实现用户认证授权，用户角色，权限分配。前端页面为极简模式，没有任何css。
![输入图片说明](https://foruda.gitee.com/images/1683948205081038356/2ddb74b6_12432892.gif "user_center.gif")

#### 软件架构
基本框架：springboot，security，mybatisPlus实现用户登录认证授权，以及简单的用户，角色，菜单管理。


#### 安装教程

1.  首先执行sql文件
2.  修改application.yml中的数据库配置，改成自己的数据库，用户名和密码
3.  启动项目，在浏览器输入http://localhost:9011/，即可进入首页。


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
